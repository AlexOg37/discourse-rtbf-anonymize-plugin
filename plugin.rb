# name: rtbf-anonymize-plugin
# version: 0.1.0
 
enabled_site_setting :rtbf_anonymize_plugin_enabled
 
after_initialize do
  module ::RtbfAnonymizePlugin
    def self.plugin_name
      'rtbf-anonymize-plugin'
    end
 
    class Engine < ::Rails::Engine
      engine_name ::RtbfAnonymizePlugin.plugin_name
      isolate_namespace RtbfAnonymizePlugin
    end
  end
 
  require_dependency "application_controller"
 
  class RtbfAnonymizePlugin::ApiController < ::ApplicationController
    requires_plugin RtbfAnonymizePlugin.plugin_name
    before_action :fetch_user
 
    def anonymize
      guardian.ensure_can_anonymize_user!(@user)
      if user = UserAnonymizer.new(@user, current_user, anonymize_ip: '0.0.0.0').make_anonymous
        render json: success_json.merge(username: user.username)
      else
        render json: failed_json.merge(user: AdminDetailedUserSerializer.new(user, root: false).as_json)
      end
    end
 
    def fetch_user
      @user = User.find_by(id: params[:user_id])
      raise Discourse::NotFound unless @user
    end
  end
 
  RtbfAnonymizePlugin::Engine.routes.draw do
    put ":user_id/anonymize" => "api#anonymize"
  end
 
  Discourse::Application.routes.append do
    mount ::RtbfAnonymizePlugin::Engine, at: "admin/plugins/anonymizer", constraints: AdminConstraint.new
  end
end